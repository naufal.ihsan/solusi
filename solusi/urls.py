"""solusi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.urls import path, include
from main.views import *
from solusi import settings

urlpatterns = [
    path('', homepage, name='home'),
    path('about/', about, name='about'),
    path('consultation/', consultation, name='consultation'),
    path('consultation/detail/<int:pk>/', consultation_detail, name='consultation_detail'),
    path('consultation/<slug:category>/', consultation_category, name='consultation_category'),
    path('training/', training, name='training'),
    path('training/<slug:category>/', training_category, name='training_category'),
    path('training/detail/<int:pk>/', training_detail, name='training_detail'),
    path('contact/', contact, name='contact'),
    path('gallery/', gallery, name='gallery'),

    # ADMIN
    path('account/', include('django.contrib.auth.urls')),
    path('admin/', admin, name='admin'),
    path('admin/client/', client, name='client'),
    path('admin/about/', admin_about, name='admin_about'),
    path('admin/consultation/', admin_consultation, name='admin_consultation'),
    path('admin/training/', admin_training, name='admin_training'),
    path('admin/message/', message, name='message'),
    path('admin/gallery/', admin_gallery, name='admin_gallery'),

    # CRUD ADMIN
    path('admin/addattr/', add_attr, name='add_attr'),
    path('admin/client/addclient/', add_client, name='addclient'),
    path('admin/consultation/addservice/', add_consultation_service, name='addservice'),
    path('admin/training/addservice/', add_training_service, name='addservicetraining'),
    path('admin/gallery/addimage/', add_image, name='addimage'),
    path('admin/message/delete/<int:pk>/', delete_message, name='delete_msg'),

    # CRUD MAIN
    path('contact/post/', post_message, name='post_msg'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
