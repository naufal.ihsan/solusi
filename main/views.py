from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from main.helpers import model_to_json
from main.models import Client, Message, Gallery, Consultation, Training, Attributes

response = {}


# Create your views here.
def homepage(request):
    if Attributes.objects.all().count() >= 3:
        response['advantages'] = Attributes.objects.all().filter(category__contains='Advantages')
        response['benefits'] = Attributes.objects.all().filter(category__contains='Benefit')
        response['strengths'] = Attributes.objects.all().filter(category__contains='Strength')

    if Consultation.objects.all().count() >= 3:
        response['consultations'] = Consultation.objects.all()[0:3]

    if Training.objects.all().count() >= 3:
        response['trainings'] = Training.objects.all()[0:3]

    if Client.objects.all().count() >= 13:
        response['clientsRow1'] = Client.objects.all()[0:5]
        response['clientsRow2'] = Client.objects.all()[5:9]
        response['clientsRow3'] = Client.objects.all()[9:13]
    return render(request, 'content/index.html', response)


def about(request):
    if Attributes.objects.all().count() >= 3:
        response['missions'] = Attributes.objects.all().filter(category__contains='Misi')
        response['vissions'] = Attributes.objects.all().filter(category__contains='Visi')

    if Client.objects.all().count() >= 13:
        response['clientsRow1'] = Client.objects.all()[0:5]
        response['clientsRow2'] = Client.objects.all()[5:9]
        response['clientsRow3'] = Client.objects.all()[9:13]
    return render(request, 'content/about.html', response)


def consultation(request):
    consultation = Consultation.objects.all()
    paginator = Paginator(consultation, 3)

    page = request.GET.get('page')
    response['consultations'] = paginator.get_page(page)
    return render(request, 'content/consultation.html', response)


def consultation_category(request, category):
    consultation = Consultation.objects.all().filter(categories__contains=category)
    paginator = Paginator(consultation, 3)

    page = request.GET.get('page')
    response['consultations'] = paginator.get_page(page)
    return render(request, 'content/consultation.html', response)


def consultation_detail(request, pk):
    consultation = Consultation.objects.get(pk=pk)
    response['consultation'] = consultation
    return render(request, 'content/detail_consultation.html', response)


def training(request):
    training = Training.objects.all()
    paginator = Paginator(training, 3)

    page = request.GET.get('page')
    response['trainings'] = paginator.get_page(page)
    return render(request, 'content/training.html', response)


def training_category(request, category):
    training = Training.objects.all().filter(categories__contains=category)
    paginator = Paginator(training, 3)

    page = request.GET.get('page')
    response['trainings'] = paginator.get_page(page)
    return render(request, 'content/training.html', response)


def training_detail(request, pk):
    training = Training.objects.get(pk=pk)
    response['training'] = training
    return render(request, 'content/detail_training.html', response)


def gallery(request):
    response['gallery'] = Gallery.objects.all()
    return render(request, 'content/gallery.html', response)


def contact(request):
    return render(request, 'content/contact.html', response)


@login_required(login_url='/account/login')
def admin(request):
    response['advantages'] = Attributes.objects.all().filter(category__contains='Advantages')
    response['benefits'] = Attributes.objects.all().filter(category__contains='Benefit')
    response['strengths'] = Attributes.objects.all().filter(category__contains='Strength')
    return render(request, 'admin/index.html', response)


@login_required(login_url='/account/login')
def admin_about(request):
    response['vissions'] = Attributes.objects.all().filter(category__contains='Visi')
    response['missions'] = Attributes.objects.all().filter(category__contains='Misi')
    return render(request, 'admin/about.html', response)


@login_required(login_url='/account/login')
def client(request):
    response['clients'] = Client.objects.all()
    return render(request, 'admin/clients.html', response)


@login_required(login_url='/account/login')
def admin_consultation(request):
    response['services'] = Consultation.objects.all()
    return render(request, 'admin/consultation.html', response)


@login_required(login_url='/account/login')
def admin_training(request):
    response['services'] = Training.objects.all()
    return render(request, 'admin/training.html', response)


@login_required(login_url='/account/login')
def message(request):
    response['messages'] = Message.objects.all()
    return render(request, 'admin/message.html', response)


@login_required(login_url='/account/login')
def admin_gallery(request):
    response['gallery'] = Gallery.objects.all()
    return render(request, 'admin/gallery.html', response)


@csrf_exempt
def add_attr(request):
    if request.method == 'POST':
        print(request.POST)
        name = request.POST['name']
        category = request.POST['category']

        attribute = Attributes(name=name, category=category)

        attribute.save()

        if category == 'Visi' or category == 'Misi':
            return HttpResponseRedirect('/admin/about/')

        return HttpResponseRedirect('/admin/')

    return HttpResponseForbidden('allowed only via POST')


@csrf_exempt
def add_client(request):
    if request.method == 'POST' and request.FILES:
        name = request.POST['name']
        position = request.POST['position']
        clientImg = request.FILES['imageUpload']

        client = Client.objects.filter(position=position).exists()

        if client:
            Client.objects.filter(position=position).delete()

        client = Client(position=position, name=name,
                        image=clientImg)
        client.save()

        return HttpResponseRedirect('/admin/client/')

    return HttpResponseForbidden('allowed only via POST')


@csrf_exempt
def add_image(request):
    if request.method == 'POST' and request.FILES:
        image = request.FILES['imageUpload']
        position = request.POST['position']
        description = request.POST['description']
        story = request.POST['story']

        img = Gallery.objects.filter(position=position).exists()

        if img:
            Gallery.objects.filter(position=position).delete()

        image = Gallery(image=image, position=position, description=description, story=story)
        image.save()

        return HttpResponseRedirect('/admin/gallery/')

    return HttpResponseForbidden('allowed only via POST')


@csrf_exempt
def add_consultation_service(request):
    if request.method == 'POST' and request.FILES:
        name = request.POST['name']
        service = request.POST['service']
        description = request.POST['description']
        advantage = request.POST['advantage']
        categories = request.POST.getlist('categories')
        image = request.FILES['imageUpload']

        all_category = ''.join(str(elem) + ',' for elem in categories)

        consultation = Consultation(service=service, name=name, description=description, image=image,
                                    advantage=advantage, categories=all_category)

        consultation.save()

        return HttpResponseRedirect('/admin/consultation/')

    return HttpResponseForbidden('allowed only via POST')


@csrf_exempt
def add_training_service(request):
    if request.method == 'POST' and request.FILES:
        name = request.POST['name']
        service = request.POST['service']
        description = request.POST['description']
        advantage = request.POST['advantage']
        categories = request.POST.getlist('categories')
        image = request.FILES['imageUpload']

        all_category = ''.join(str(elem) + ',' for elem in categories)

        training = Training(service=service, name=name, description=description, image=image, advantage=advantage,
                            categories=all_category)

        training.save()

        return HttpResponseRedirect('/admin/training/')

    return HttpResponseForbidden('allowed only via POST')


@csrf_exempt
def post_message(request):
    if request.method == 'POST':
        sender = request.POST['sender']
        email = request.POST['email']
        subject = request.POST['subject']
        body = request.POST['body']

        message = Message(sender=sender, email=email, subject=subject, body=body)
        message.save()

        data = model_to_json(message)
        return HttpResponse(data)

    return HttpResponseForbidden('allowed only via POST')


def delete_message(request, pk):
    message = Message.objects.filter(pk=pk).exists()

    if message:
        message = Message.objects.filter(pk=pk)
        message.delete()

    return HttpResponseRedirect('/admin/message/')
