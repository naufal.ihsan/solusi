from django.db import models


# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='client_logo/')
    position = models.IntegerField()

    class Meta:
        ordering = ["position"]


class Message(models.Model):
    sender = models.CharField(max_length=255)
    email = models.EmailField()
    subject = models.CharField(max_length=255)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["date"]


class Gallery(models.Model):
    image = models.ImageField(upload_to='gallery/')
    position = models.IntegerField(default=0)
    description = models.CharField(max_length=255)
    story = models.TextField(default="")

    class Meta:
        ordering = ["position"]


class Consultation(models.Model):
    service = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='consultation_service/')
    description = models.TextField()
    advantage = models.TextField(default="")
    categories = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["date"]


class Training(models.Model):
    service = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='consultation_service/')
    description = models.TextField()
    advantage = models.TextField(default="")
    categories = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["date"]


class Attributes(models.Model):
    category = models.CharField(max_length=255)
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ["name"]
